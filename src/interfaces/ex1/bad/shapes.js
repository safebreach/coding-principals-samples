/**
 * Created by dan on 9/8/15.
 */

"use strict";

class Shape {
    contructor() {

    }
    area() {
        return 0;
    }
    volume() {
        return 0;
    }
}


class Circle extends Shape{
    constructor(radius) {
        super();
        this.radius = radius;
    }

    area() {
        return Math.PI * Math.pow(this.radius, 2);
    }
}

class Square extends Shape{
    constructor(length) {
        super();
        this.length = length;
    }

    area() {
        return Math.pow(this.length, 2);
    }
}

class Cuboid extends Shape {
    constructor(length) {
        super();
        this.length = length;
    }

    volume() {
        return Math.pow(this.length, 3);
    }
}

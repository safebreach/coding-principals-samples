/**
 * Created by dan on 9/8/15.
 */

"use strict";

class Shape {
    contructor() {

    }
}

class FlatShape extends Shape{
    constructor() {
        super();
    }
    area() {
        return 0;
    }
}

class SolidShape extends Shape{
    constructor() {
        super();
    }
    volume() {
        return 0;
    }
}

class Circle extends FlatShape{
    constructor(radius) {
        super();
        this.radius = radius;
    }

    area() {
        return Math.PI * Math.pow(this.radius, 2);
    }
}

class Square extends FlatShape{
    constructor(length) {
        super();
        this.length = length;
    }

    area() {
        return Math.pow(this.length, 2);
    }
}

class Cuboid extends SolidShape {
    constructor(length) {
        super();
        this.length = length;
    }

    volume() {
        return Math.pow(this.length, 3);
    }
}

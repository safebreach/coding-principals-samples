/**
 * Created by dan on 9/8/15.
 */

"use strict";

class Circle {
    constructor(radius) {
        this.radius = radius;
    }
}

class Square {
    constructor(length) {
        this.length = length;
    }
}

class AreaCalculator {

    constructor(shapes) {
            this.shapes = shapes;
    }

    sum() {
        // logic to sum the areas
        return 8;
    }

    output() {
        console.log ("Sum of the areas of provided shapes: " + this.sum());
    }
}

var shapes = [
    new Circle(2),
    new Square(5),
    new Square(6)
];

var areas = new AreaCalculator(shapes);
areas.output();
/**
 * Created by dan on 9/8/15.
 */

"use strict";

class Circle {
    constructor(radius) {
        this.radius = radius;
    }
}

class Square {
    constructor(length) {
            this.length = length;
    }
}

class AreaCalculator {

    constructor(shapes) {
        this.shapes = shapes;
    }

    sum() {
        var areas = 0;
        this.shapes.forEach(function(shape) {
            if(shape instanceof Square) {
                areas = areas + (Math.pow(shape.length, 2));
            } else if(shape instanceof Circle) {
                areas = areas + (Math.PI * Math.pow(shape.radius, 2));
            }
        });
        return areas;
    }
}

class AreasLogger {
    constructor (areaCalculator) {
        this.areaCalculator = areaCalculator;
    }

    output() {
        console.log ("Sum of the areas of provided shapes: " + this.areaCalculator.sum());
    }
}

var shapes = [
    new Circle(2),
    new Square(5),
    new Square(6)
];

var areasLogger = new AreasLogger(new AreaCalculator(shapes));
areasLogger.output();
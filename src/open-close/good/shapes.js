/**
 * Created by dan on 9/8/15.
 */

"use strict";

class Circle {
    constructor(radius) {
        this.radius = radius;
    }

    area() {
        return Math.PI * Math.pow(this.radius, 2);
    }
}

class Square {
    constructor(length) {
            this.length = length;
    }

    area() {
        return Math.pow(this.length, 2);
    }
}

class AreaCalculator {

    constructor(shapes) {
        this.shapes = shapes;
    }

    sum() {
        var areas = 0;
        this.shapes.forEach(function(shape) {
            areas = areas + shape.area();
        });
        return areas;
    }
}

class AreasLogger {
    constructor (areaCalculator) {
        this.areaCalculator = areaCalculator;
    }

    output() {
        console.log ("Sum of the areas of provided shapes: " + this.areaCalculator.sum());
    }
}

var shapes = [
    new Circle(2),
    new Square(5),
    new Square(6)
];

var areasLogger = new AreasLogger(new AreaCalculator(shapes));
areasLogger.output();
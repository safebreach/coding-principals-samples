/**
 * Created by dan on 9/8/15.
 */

"use strict";

class Circle {
    constructor(radius) {
        this.radius = radius;
    }

    area() {
        return Math.PI * Math.pow(this.radius, 2);
    }
}

class Square {
    constructor(length) {
        this.length = length;
    }

    area() {
        return Math.pow(this.length, 2);
    }
}

class SumCalculator {

    constructor(shapes, fn) {
        this.shapes = shapes;
        this.fn = fn;
    }

    sum() {
        var areas = 0;
        this.shapes.forEach(function(shape) {
            areas = areas + shape[this.fn]();
        });
        return areas;
    }
}

class AreaCalculator extends SumCalculator{

    constructor(shapes) {
        super (shapes, 'area');
    }

    getAreaList() {
        var areas = [];
        this.shapes.forEach(function(shape) {
            areas.push(shape.area());
        });
        return areas;
    }
}

class SumsLogger {
    constructor (sumCalculator) {
        this.sumCalculator = sumCalculator;
    }

    output() {
        console.log ("Sum of "+this.sumCalculator.fn+" of provided shapes: " + this.sumCalculator.sum());
    }
}

var shapes = [
    new Circle(2),
    new Square(5),
    new Square(6)
];

var sumLogger = new SumsLogger(new AreaCalculator(shapes));
sumLogger.output();